#!/usr/bin/python3

import array
import sys

fname = sys.argv[1]

file = open('{fname}.ppm'.format(fname=fname), 'r')

colors = file.read().splitlines()

image = open('{fname}.asm'.format(fname=fname), 'w')

wh = colors[2].split(' ')
w = int(wh[0])
h = int(wh[1])

for i in range(0,h):
    for j in range(0,w):
        index = ((w)*i+j)*3+4
        r = int(colors[index+0])
        g = int(colors[index+1])
        b = int(colors[index+2])

        r=r<<16
        g=g<<8
        b=b

        c = r+g+b

        image.write(hex(c)+ ', ')
    image.write('\n')

file.close()
image.close()
        


