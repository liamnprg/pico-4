# CSCB58 School Final Project
This is my final project for CSCB58, a version of Celeste for the MARS MIPS assembler.

## How to run:

Run game.asm with the mars assembler configured as follows:
* No branch delays
* Bitmap display with 4x4 pixel size and 256x256 image width/height

## Virus warning

MIPS asm files have access to the hard drive, be careful with downloading and running asm files in MARS. This program is not a virus, and it does not utilize the hard drive, even for assets.

## Copyright

This code is licensed under GPL-3, asset credit goes to the Celeste game design team, specifically the color palette and the charlotte asset.

